```@meta
CurrentModule = QuantumESPRESSOBase.PWscf
```

# `QuantumESPRESSOBase.PWscf` module

```@contents
Pages = ["PWscf.md"]
Depth = 3
```

## Types

```@docs
Lattice
ControlNamelist
SystemNamelist
ElectronsNamelist
IonsNamelist
CellNamelist
DosNamelist
BandsNamelist
AtomicSpecies
AtomicSpeciesCard
AtomicPosition
AtomicPositionsCard
CellParametersCard
KMeshCard
GammaPointCard
SpecialPointsCard
PWInput
```

## Methods

```@docs
isrequired
isoptional
cellvolume
convertoption
getpseudodir
listpotentials
getxmldir
listwfcfiles
```
