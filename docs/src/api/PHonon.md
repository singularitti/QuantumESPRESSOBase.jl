```@meta
CurrentModule = QuantumESPRESSOBase.PHonon
```

# `QuantumESPRESSOBase.PHonon` module

```@contents
Pages = ["PHonon.md"]
Depth = 3
```

## Types

```@docs
PhNamelist
Q2rNamelist
MatdynNamelist
DynmatNamelist
```

## Methods

```@docs
relayinfo
```
